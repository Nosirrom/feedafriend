package com.nosirrom.feedafriend;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.EntityInteractEvent;

import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class FeedAFriendEvent {
	
	Logger log = FeedAFriend.logger;
	
	@SubscribeEvent
	public void interact(EntityInteractEvent event){
		if(event.target instanceof EntityPlayerMP){ //checks if you right click another player.
			
			log.debug("Aha there is a player there!");
			EntityPlayer friend = (EntityPlayer) event.target; //get playerEntity from targetName
			
			if(event.entityPlayer.getHeldItem() != null && event.entityPlayer.getHeldItem().getItem() != null && event.entityPlayer.getHeldItem().getItem() instanceof ItemFood){

					ItemStack food = event.entityPlayer.getHeldItem();
					log.debug("This is an instance of itemFood! Checking if friend needs food.");
					if(friend.canEat(false)){
						
						log.info("friend needs food! Feeding Food to friend...");
						log.debug("food is: " + food.getDisplayName());
						food.onFoodEaten(friend.worldObj, friend); //give possible potion effects to friend
						log.info("You just fed your friend!");
					}else {
						
						log.info("Your friend isn't hungry; they don't need food");
					}
				}else {
					
					log.debug("Item is null or not instance of food");
				}
			} else {
				
			log.debug("Unidentified entityInteractEvent");
			}
		}
}
