package com.nosirrom.feedafriend;

import net.minecraftforge.common.MinecraftForge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;

@Mod(
		modid = "FeedAFriend",
		name = "Feed A Friend",
		version = "1.7.10-1.0.4"
		
		)

public class FeedAFriend {
	
	@Instance("FeedAFriend")
	public static FeedAFriend instance;
	public static Logger logger = LogManager.getLogger("FeedAFriend");
	
	@EventHandler
	public void init(FMLInitializationEvent event){
		MinecraftForge.EVENT_BUS.register(new FeedAFriendEvent());
		logger.info("FeedAFriend mod Initialized");
	}
}
